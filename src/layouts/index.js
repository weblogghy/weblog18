import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Favicon from '../favicon.ico'

import './tachyon/css/tachyons.css'
import './custom.css'

const Layout = ({ children, data }) => (
  <div>
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'description', content: 'WeBlog Guwahati 2018' },
        { name: 'keywords', content: 'blogging,\
            guwahati, assam, northeast, event' },
      ]}
    >
      <link rel="icon" type="image/png" href={Favicon} sizes="16x16" />
    </Helmet>
    <div
      style={{
        margin: '0 auto', maxWidth: 960, padding: '0 0.2em', paddingTop: 0, fontFamily: 'Roboto Mono,monospace'
      }}
    >
      {children()}
    </div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
