import React from 'react'
import Link from 'gatsby-link'
import Header from '../components/Header'
import Footer from '../components/footer'

const SponsorPage = () => (
  <div>
    <Header/>
    <article className="cf pv3">
        Thankyou for your interest in sponsoring,
        <br/>
        <br/>
        Download the <a href="https://drive.google.com/file/d/1TaBeOlZTjY5UnlX1ZFvkqZAGhkPtv8Td/view" 
        className="f3 bg-yellow black link">sponsor kit</a>
        <br/>
        <br/>
        Please feel free to contact us directly at:
        <li> + 91-7002312940 (Indrajeet)</li>
        <li> + 91-8876425871 (Angshuman) </li>
        <li> weblogcon@gmail.com </li>
    </article>
    <Footer/>
  </div>
)

export default SponsorPage
