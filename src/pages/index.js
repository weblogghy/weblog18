import React from 'react'
import Link from 'gatsby-link'
import Header from '../components/Header'
import Hero from '../components/Hero'
import TargetAud from '../components/TargetAud'
import Speakers from '../components/Speakers'
import Schedule from '../components/Schedule'
import Sponsors from '../components/sponsors'
import Supporter from '../components/supporters'
import Archive from '../components/archive'
import Footer from '../components/footer'
import Ticket from '../components/tickets'
import Banner from '../components/NEAbanner'

const IndexPage = () => (
  <div className="w-90 w-100-ns center">
    <Header/>
    <Hero/>
    {/*<Banner/>*/}
    <TargetAud/>
    <Speakers/>
    <Schedule/>
    <Ticket/>
    <Sponsors/>
    {/*<Supporter/>*/}
    <Archive/>
    <Footer/>
  </div>
)

export default IndexPage
