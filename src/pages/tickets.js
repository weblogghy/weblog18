import React from 'react'
import Link from 'gatsby-link'
import Header from '../components/Header'
import Footer from '../components/footer'

const TicketPage = () => (
  <div>
    <Header/>
    <article className="cf pv1">
     <h3>What your ticket to Weblog Guwahati 2018 includes:</h3>
     <dl className="lh-title mt0">
       <dt className="f6 b">Food?</dt>
       <dd className="ml0">Covered for both the days.</dd>
       <dt className="f6 b mt2">AC</dt>
       <dd className="ml0">Yes</dd>
       <dt className="f6 b mt2">Ola Discounts</dt>
       <dd className="ml0">We'll text you cupons before event.</dd>
       <dt className="f6 b mt2">Favorite Food</dt>
       <dd className="ml0">Cheese Pizza</dd>
       <dt className="f6 b mt2">Least Favorite Flavor</dt>
       <dd className="ml0">Cherry</dd>
       <dt className="f6 b mt2">Favorite Hobby</dt>
       <dd className="ml0">Eating Cheese Pizza</dd>
     </dl>
    <p className="db bg-black white pv2 pa1">Early bird tickets(50) now on sale at ₹1000</p>
    <a 
      className="f6 link dim ph4 pv3 mb2 dib black bg-yellow"  
      href="#">TAKE MY MONEY.</a>
    </article>
    <Footer/>
  </div>
)

export default TicketPage
