import React from 'react'
import Link from 'gatsby-link'
import CutCircle from '../CutCircle.svg'
//import CutCircle from '../CutCircle.png'

const Tickets = () => (
    <div id="tickets">
      <div className="mw9 mt5">
        <div className="cf">
          <div className="fl w-100 w-15-ns">
            <div className="bg-white-ns ttu b tracked-mega">
             Buy Tickets
            </div>
          </div>
          <div className="fl w-100 w-40-ns cf">
            <article className="cf pv1">
             <h3 className="ma0 f6 f5-ns wb-grey-2">Your ticket to Weblog includes</h3>
             <dl className="lh-title mt0 mt4">
               <dt className="f4">Food?</dt>
               <dd className="ml0 pb3 pt1 f6 wb-grey-2">Covered for both the days.</dd>
               <dt className="f4">Goodies?</dt>
               <dd className="ml0 pb3 pt1 f6 wb-grey-2">Blogging Kit + Goodies</dd>
               <dt className="f4">Certificates?</dt>
               <dd className="ml0 pb3 pt1 f6 wb-grey-2">Yup.</dd>
               <dt className="f4">Fun</dt>
               <dd className="ml0 pb3 pt1 f6 wb-grey-2">It's all about that.</dd>
               <dt className="f4">Where?</dt>
               <dd className="ml0 pb3 pt1 f6 wb-grey-2">
                Indian Institute of Entrepreneurship, Near Game Village, Lalmati, Guwahati.
               </dd>
             </dl>
            </article>
          </div>
          <div className="fl w-100 w-40-ns cf">
          <p className="db dn-ns f4 wb-grey-1 ma0 lh-title">
            Tickets on sale at <span className="bg-yellow black">₹1200</span>
            </p>
          <a 
            className="mt4 f3 link dim ph4 db dn-ns pv3 dib black bg-wb-blue ttc z-1" 
            href="https://docs.google.com/forms/d/e/1FAIpQLSeLpxejpx_4EJvSgOL3IZY3mottlVlv9z0G4f0CcMemiJAUUw/viewform"
            >Get tickets <small className="f6 db"></small></a>
          <article className="dn db-ns mw5 mw6-ns center">
            <div className="aspect-ratio aspect-ratio--1x1 mb4">
              <div className="aspect-ratio--object cover bg-black white">
                <p className="f3 wb-grey-3 ma0 ph4 pt4 pb4 lh-title">
                  Tickets on sale at <span className="white">₹1200</span>
                  </p>
                <div className="ma0 ph4 pt3 pb5 mw100 tc relative">
                    <img 
                       className="w-50 absolute top-0 mv0 center bottom-0 left-0 right-0 ultra-no-zindex"
                       src={CutCircle}/>
                    <a 
                      className="f3 link dim ph4 pv3 mb1 mt5 dib black bg-wb-blue ttc z-1" 
                      target="_blank"
                      href="https://docs.google.com/forms/d/e/1FAIpQLSeLpxejpx_4EJvSgOL3IZY3mottlVlv9z0G4f0CcMemiJAUUw/viewform"
                      >Get tickets <small className="f6 db"></small></a>
                </div>
              </div>
            </div>
          </article>
          </div>
        </div>
      </div>
    </div>
)

export default Tickets