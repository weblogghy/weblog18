import React from 'react'
import Link from 'gatsby-link'
import Pie from '../pie_chart.svg'

class Aud extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
    this.toggle = this.toggle.bind(this);
  }
  toggle(){
    this.setState({
      open: !this.state.open
    })
  }
  render(){
    return (
    <article className="mw6-ns br3 hidden pb2 pb3-ns">
      <p 
        onClick={this.toggle}
        className="f4-ns f5 mv0 pv2-ns pv0 di">{this.props.heading}</p>
      <span 
        onClick={this.toggle}
        className="di dn-ns"> {`${this.state.open?"▴":"▾"}`}</span>
      <p className={`f6 f6-ns lh-copy measure wb-grey-2 mt1 ${this.state.open?"db":"dn"} db-ns`}>
        {this.props.description}
      </p>
    </article>
    )
  }
}

const TargetAud = () => (
    <div className="">
      <div className="mw9">
        <div className="cf">
          <div className="fl w-100 w-15-ns">
            <div className="bg-white-ns ttu b tracked-mega">
            For Who?
            </div>
            <img className="dn db-l pt4 h4 w4 absolute left--110px" src={Pie}/>
          </div>
            <div className="fl w-100 w-40-ns pt2">
              <div className="pa2-ns pa0">
                <Aud heading="Personal Bloggers" description="
                Share life experiences, feelings and thoughts.
                "/>
                <Aud heading="Social Media Influencers" description="
                  Social media users who have established credibility
                  in a specific industry
                "/>
                <Aud heading="Content Managers" description="
                Get paid to create content or manage website
                content for online businesses.
                "/>
                <Aud heading="Professional Bloggers" description="
                  Make money online from blogging
                "/>
              </div>
            </div>
            <div className="fl w-100 w-40-ns">
              <div className="pa2-ns pa0">
                <Aud heading="Entrepreneurs" description="
                  Entrepreneurs or startups who are looking to
                  create an online audience and reach out to people.
                "/>
                <Aud heading="Affiliate Marketers" description="
                Promote products online and make profit.
                "/>
                <Aud heading="Social Media Managers" description="
                  Run social media accounts for corporates
                "/>
                <Aud heading="Cryptocurrency Enthusiasts" description="
                  People who are interested in making profits from
                  trading bitcoins and other crypto.
                "/>
              </div>
          </div>
        </div>
      </div>
    </div>
)

export default TargetAud
