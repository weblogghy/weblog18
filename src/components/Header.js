import React from 'react'
import Link from 'gatsby-link'
import Logo from '../logo.svg'

const Header = () => (
<header className="w-100 pv3 ttu">
  <div className="db dt-ns mw9 center w-100">
    <div className="dib dtc-ns v-mid tl w-20">
      <a href="/" className="db" title="WeBlog"> 
        <img 
        className="h3 b3"
        src={Logo}/>
      </a>
    </div>
    <Link
      to="/#tickets"
      className="fr f6 link dim ph3 pv2 mb2 white bg-black dn-ns">
      Get Tickets
      </Link>
    <nav className="db dtc-ns v-mid w-100 tl tr-ns mt2 mt0-ns pt3 pt0-ns">
      <a title="Documentation" href="/#speakers"
          className="f6 fw6 hover-blue link wb-grey-2 mr2 mr3-m mr4-l dib">
        Speakers
      </a>
      <a title="Components" href="/#schedule"
          className="f6 fw6 hover-blue link wb-grey-2 mr2 mr3-m mr4-l dib">
        Schedule
      </a>
      <a title="Gallery of sites built with Tachyons" href="/#sponsors"
          className="f6 fw6 hover-blue link wb-grey-2 mr2 mr3-m mr4-l dib">
        Sponsors
      </a>
      <a title="Resources" href="/#archive"
          className="f6 fw6 hover-blue link wb-grey-2 mr2 mr3-m mr4-l dib">
        Archive
      </a>
      <Link to="/#tickets"
          className="f6 link ph3 pv2 mb2 white bg-black dn dib-ns change-2-yellow-on-hover">
        Get Tickets
      </Link>
    </nav>
  </div>
</header>
)

export default Header
