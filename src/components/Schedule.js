import React from 'react'
import Link from 'gatsby-link'

const Session = ({hr,mn,name}) => (
    <div className="flex w-100">
        <div className="f4 pa3 bg-black i white di">
            <h4 className="ma0 flex-1">{hr}</h4>
            <h4 className="ma0">{mn}</h4>
        </div>
        <div className="f6 di ph3 wb-grey-2">{name}</div>
    </div>
)

const Schedule = () => (
    <div id="schedule">
      <div className="mw9 mt5">
        <div className="cf">
          <div className="fl w-100 w-15-ns">
            <div className="bg-white-ns ttu b tracked-mega">
             Schedule
            </div>
          </div>
          <div className="fl w-100 w-40-ns cf">
            <h1 className="f2-ns f3 fw1 pb2 ma0 i ttu">Day 1</h1>
            <Session hr="09" mn="30" name="Inauguration and speech by chief guest"/>
            <Session hr="10" mn="00" name="Blogger success story"/>
            <Session hr="10" mn="15" name="Evan’s motivational speech"/>
            <Session hr="10" mn="30" name="Tea break"/>
            <Session hr="10" mn="45" name="How to write great content by Aritra"/>
            <Session hr="11" mn="30" name="Setting up your first blog by Indrajeet"/>
            <Session hr="12" mn="30" name="Get traffic to your blog by Pushpanjalee"/>
            <Session hr="13" mn="30" name="Lunch Break"/>
            <Session hr="14" mn="15" name="Panel on fashipn bloggin in NE"/>
            <Session hr="15" mn="15" name="Making a successful Youtube channel by Dhananjay Bhosle"/>
            <Session hr="16" mn="15" name="Q&A"/>

          </div>
          <div className="fl w-100 w-40-ns cf">
            <h1 className="f2-ns f3 fw1 pb2 ma0 i ttu">Day 2</h1>
            <Session hr="09" mn="00" name="Lighting talk on cracking Youtube growth secret"/>
            <Session hr="09" mn="30" name="Tea Break"/>
            <Session hr="09" mn="45" name="10 lessons I learned while building my first million dollar blog by Mohit Kumar"/>
            <Session hr="10" mn="45" name="Introduction to cryptocurrency by Indrajeet"/>
            <Session hr="11" mn="45" name="What is it like to be an Internet marketer by Almas Malik"/>
            <Session hr="12" mn="30" name="Making some quick money off Instagram - Indrajeet"/>
            <Session hr="13" mn="15" name="Lunch"/>
            <Session hr="14" mn="00" name="Northeast Creator Awards 2018"/>
            <Session hr="14" mn="45" name="Panel on food blogging in NE"/>
            <Session hr="15" mn="15" name="Earning 2 lakhs per month by affiliate marketing - Siddharth Bagga"/>
            <Session hr="16" mn="30" name="Q&A"/>
          </div>
        </div>
      </div>
    </div>
)

export default Schedule