import React from 'react'
import Link from 'gatsby-link'
import Spk from '../speakers.svg'

// Sperakers
import Unknown from '../unknown.jpg'
import sp1 from '../speakers/sp1.jpg'
import sp2 from '../speakers/sp2.jpg'
import sp3 from '../speakers/sp3.jpg'
import sp4 from '../speakers/sp4.png'
import sp5 from '../speakers/sp5.png'
import sp6 from '../speakers/sp6.png'
import sp7 from '../speakers/sp7.png'
import sp8 from '../speakers/sp8.jpg'
import sp9 from '../speakers/sp9.jpg'

class Person extends React.Component {
  constructor(props) {
    super(props);
    this.state = {modalOpen: false};
    this.toggle = this.toggle.bind(this);
  }
  toggle(){
    this.setState({
      modalOpen: !this.state.modalOpen
    })
  }
  render(){
    const {name, desg, photo, talk, desc} = this.props
    const {modalOpen} = this.state
    return (
      <div>

        <div className={`${modalOpen?"flex":"dn"} fixed left-0 top-0 w-100 h-100 ph1 pv5 pa5-ns z-100`}>
          <div className="bg-black white flex flex-column h-100 w-90 w-80-ns center pa3 ph5-ns pt5-ns pb1-ns">
            <p className="f5 mb0">{name}</p>
            <p className="f5">{desg} </p>
            <p className="f6">{desc}</p>
            <div
            onClick={this.toggle}
            className="db self-end pointer w-100 pa2 tc ttu center bg-wb-yellow black">Close</div>
          </div>
        </div>

        <div className="pointer fl w-50 w-25-m w-25-l" onClick={this.toggle}>
         <div className="db link tc">
          <img src={photo} className="w-100 db outline black-10"/>
          <dl className="mt2 f6 lh-copy">
            <dt className="clip">{name}</dt>
            <dd className="ml0 black truncate w-100">{name}</dd>
            <dt className="clip">{desc}</dt>
            <dd className="ml0 gray truncate w-100">{desg}</dd>
          </dl>
        </div>
        </div>
      </div>
    )
  }
}

const Speakers = () => (
    <div id="speakers">
      <div className="mw9 mt4">
        <div className="cf">
          <div className="fl w-100 w-15-ns">
            <div className="bg-white-ns ttu b tracked-mega">
             Speakers
            </div>
            <img className="dn db-l pt4 h4 w4 absolute left--110px" src={Spk}/>
          </div>
          <div className="fl w-100 w-85-ns">
            <Person 
              name="Aritra Sarkhel" 
              desg="TheBigScope" 
              photo={sp1} 
              talk="How to write great Content"
              desc="Aritra was an investigative Technology journalist with India's largest Business Daily - Economic Times, he helped create the Digital Video platform for one the oldest Tech Media firms - IDG Media and currently building India's first Post Cable Regional Network - 24/7 Live streaming channel on News and entertainment - TheBigScope."/>
            <Person 
              name="Puspanjalee Das Dutta"
              desg="Blogger" 
              photo={sp2} 
              talk="Getting more traffic to your blog"
              desc="Dutta is a Guwahati based professional blogger who has successfully turned her blog into a six figures online business. Her blogs are read by more than 30 million people till now. She blogs about online writing and blogging. She has also won several accolades for both of her blogs. Last year she won Indian Bloggers Award  by IndiBlogger under the section Assam"/>
            <Person 
              name="Siddarth Bagga" 
              desg="Affiliate Marketer" 
              photo={sp3} 
              desc="Acknowledged as one of world's top 10 affiliate marketer by max bounty, Siddharth Bagga will be coming to WeBlog to do an exclusive session on how to earn 2 lakhs per month by affiliate marketing."/>
            <Person 
              name="Dhananjay Bhosle" 
              desg="Youtuber"
              photo={sp4} 
              desc="Dhananjay is a popular tech YouTuber from Mumbai. He have almost 3 lakhs on YouTube and other social networks. 
              He worked with various brands. He will be doing a session on how to make a career on YouTube"
              />
            <Person 
              name="Sirax Khan" 
              desg="Panelist" 
              photo={sp5} 
              desc="A Motovlogger from Guwahati, Sirax recently completed his Kashmir to kanyakumari ride. He got some awesome editing and storytelling skills."/>
            <Person 
              name="Nitushmita Barman" 
              desg="Panelist" 
              photo={sp6} 
              desc="Nitushmita Barman (Kiyashmi) is a Lifestyle blogger from northeast who runs one of the successful lifestyle blog kiyashmi.com"/>
            <Person 
              name="Devlina Bhatacharya" 
              desg="Panelist" 
              photo={sp7} 
              desc=""/>
            <Person 
              name="Mohit Kumar" 
              desg="Founder, Blogger" 
              photo={sp8} 
              desc=""/>
            <Person 
              name="Padmaja Deka" 
              desg="Panelist" 
              photo={sp9} 
              desc=""/>
          </div>
        </div>
      </div>
    </div>
)

export default Speakers
