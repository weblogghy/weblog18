import React from 'react'
import Link from 'gatsby-link'

const Hero = () => (
    <div className="w-100 fl pb5 pt5 bg-wb-grey-4">
      <div className="fl w-60">
        <h1 className="f1 f-subheadline-ns b ma0 lh-solid">
            WeBlog <br/> 2018 <br/> <span className="wb-grey-3">Guwahati</span>
        </h1>
        <h3 className="f6 f5-ns ttu wb-grey-2 db pt1">It's bigger this year. September 8 and 9, 2018</h3>
        <Link 
          to="/neca"
          className="no-underline f6 db dn-ns w-100 pv1 pl1
          bg-animate bg-black dim white br2"
        > Register your blog for #NECA
        </Link>
      </div>
      <div className="fr w-40 pv2">
        <Link to="/#tickets"
            style={{color: '#000000'}}
            className="fr dn dib-ns f3 link dim ph4 pv3 mb2 white bg-wb-yellow">
          Get Tickets ⟶
        </Link>
        <div className="dn dib-ns fl">
        Indian Institute of Entrepreneurship, Near Game Village, Lalmati, Guwahati.
        </div>
      </div>
    </div>
)

export default Hero
