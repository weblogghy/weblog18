module.exports = {
  siteMetadata: {
    title: 'WeBlog Guwahati 2018',
  },
  plugins: [
  {
    resolve: `gatsby-plugin-react-helmet`
  },
  {
    resolve: `gatsby-plugin-facebook-pixel`,
    options: {
      pixelId: '2208349452732590',
    },
  }
  ]
}
